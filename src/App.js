import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import './App.css';

import IndexView from './wizard/components/IndexView';
import InstallView from './wizard/components/InstallView';
import FinishView from './wizard/components/FinishView';
import CallBackView from './wizard/components/CallBackView';
import HelpView from './wizard/components/HelpView';

class App extends Component {
    render() {
        return (
            <div className="App full-height">
                <Switch>
                    <Route exact path="/" component={IndexView} />
                    <Route path="/index" component={IndexView} />
                    <Route path="/install" component={InstallView} />
                    <Route path="/finish/:errorMessage?" component={FinishView} />
                    <Route path="/help" component={HelpView} />
                    <Route path="/CallBack.html" component={CallBackView} />
                    <Route component={IndexView} />
                </Switch>
            </div>
        );
    }
}

export default withRouter(App);