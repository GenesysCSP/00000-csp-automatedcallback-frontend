let jsonObj = {
    "name": "CallBack_genesys-csp-testing",
    "startPageId": "a556a442-ffc0-11e4-8df6-17ce7d092df5",
    "pages": [{
            "id": "a556a442-ffc0-11e4-8df6-17ce7d092df5",
            "name": "Start Page",
            "properties": {
                "onLoadAction": {
                    "typeName": "action",
                    "value": {
                        "actionArgumentValues": []
                    }
                },
                "backgroundColor": {
                    "typeName": "text",
                    "value": ""
                }
            },
            "dataVersion": 0,
            "rootContainer": {
                "type": "verticalStackContainer",
                "properties": {
                    "margin": {
                        "typeName": "spacing",
                        "value": [5, 5, 5, 5]
                    },
                    "alignment": {
                        "typeName": "alignment",
                        "value": "start"
                    },
                    "width": {
                        "typeName": "sizing",
                        "value": {
                            "sizeType": "stretch",
                            "size": 100
                        }
                    },
                    "height": {
                        "typeName": "sizing",
                        "value": {
                            "sizeType": "stretch",
                            "size": 100
                        }
                    },
                    "visible": {
                        "typeName": "variable"
                    },
                    "backgroundColor": {
                        "typeName": "text",
                        "value": ""
                    },
                    "padding": {
                        "typeName": "spacing",
                        "value": [5, 5, 5, 5]
                    },
                    "childArrangement": {
                        "typeName": "childArrangement",
                        "value": "start"
                    },
                    "border": {
                        "typeName": "border",
                        "value": [{
                                "width": 0,
                                "style": "solid",
                                "color": "000000"
                            }, {
                                "width": 0,
                                "style": "solid",
                                "color": "000000"
                            }, {
                                "width": 0,
                                "style": "solid",
                                "color": "000000"
                            }, {
                                "width": 0,
                                "style": "solid",
                                "color": "000000"
                            }
                        ]
                    }
                },
                "children": [{
                        "type": "text",
                        "properties": {
                            "margin": {
                                "typeName": "spacing",
                                "value": [5, 5, 5, 5]
                            },
                            "alignment": {
                                "typeName": "alignment",
                                "value": "start"
                            },
                            "width": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "auto"
                                }
                            },
                            "height": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "auto"
                                }
                            },
                            "visible": {
                                "typeName": "variable"
                            },
                            "text": {
                                "typeName": "interpolatedText",
                                "value": "Interaction Details"
                            },
                            "bold": {
                                "typeName": "boolean",
                                "value": true
                            },
                            "italic": {
                                "typeName": "boolean",
                                "value": false
                            },
                            "underline": {
                                "typeName": "boolean",
                                "value": false
                            },
                            "font": {
                                "typeName": "font",
                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                            },
                            "fontSize": {
                                "typeName": "integer",
                                "value": 16
                            },
                            "justification": {
                                "typeName": "text",
                                "value": "left"
                            },
                            "backgroundColor": {
                                "typeName": "text",
                                "value": ""
                            },
                            "textColor": {
                                "typeName": "text",
                                "value": ""
                            },
                            "padding": {
                                "typeName": "spacing",
                                "value": 0
                            }
                        }
                    }, {
                        "type": "verticalStackContainer",
                        "properties": {
                            "margin": {
                                "typeName": "spacing",
                                "value": 0
                            },
                            "alignment": {
                                "typeName": "alignment",
                                "value": "start"
                            },
                            "width": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "auto"
                                }
                            },
                            "height": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "auto"
                                }
                            },
                            "visible": {
                                "typeName": "variable"
                            },
                            "backgroundColor": {
                                "typeName": "text",
                                "value": ""
                            },
                            "padding": {
                                "typeName": "spacing",
                                "value": 10
                            },
                            "childArrangement": {
                                "typeName": "childArrangement",
                                "value": "start"
                            },
                            "border": {
                                "typeName": "border",
                                "value": {
                                    "width": 0,
                                    "style": "solid",
                                    "color": "000000"
                                }
                            }
                        },
                        "children": [{
                                "type": "horizontalStackContainer",
                                "properties": {
                                    "margin": {
                                        "typeName": "spacing",
                                        "value": 0
                                    },
                                    "alignment": {
                                        "typeName": "alignment",
                                        "value": "start"
                                    },
                                    "width": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "stretch",
                                            "size": 100
                                        }
                                    },
                                    "height": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "auto"
                                        }
                                    },
                                    "visible": {
                                        "typeName": "variable"
                                    },
                                    "backgroundColor": {
                                        "typeName": "text",
                                        "value": ""
                                    },
                                    "padding": {
                                        "typeName": "spacing",
                                        "value": 10
                                    },
                                    "childArrangement": {
                                        "typeName": "childArrangement",
                                        "value": "start"
                                    },
                                    "border": {
                                        "typeName": "border",
                                        "value": {
                                            "width": 0,
                                            "style": "solid",
                                            "color": "000000"
                                        }
                                    }
                                },
                                "children": [{
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "Campaign Name:"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": true
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 11
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }, {
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "    {{dialer.campaignName}}"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 10
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }
                                ]
                            }, {
                                "type": "horizontalStackContainer",
                                "properties": {
                                    "margin": {
                                        "typeName": "spacing",
                                        "value": 0
                                    },
                                    "alignment": {
                                        "typeName": "alignment",
                                        "value": "start"
                                    },
                                    "width": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "stretch",
                                            "size": 100
                                        }
                                    },
                                    "height": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "auto"
                                        }
                                    },
                                    "visible": {
                                        "typeName": "variable"
                                    },
                                    "backgroundColor": {
                                        "typeName": "text",
                                        "value": ""
                                    },
                                    "padding": {
                                        "typeName": "spacing",
                                        "value": 10
                                    },
                                    "childArrangement": {
                                        "typeName": "childArrangement",
                                        "value": "start"
                                    },
                                    "border": {
                                        "typeName": "border",
                                        "value": {
                                            "width": 0,
                                            "style": "solid",
                                            "color": "000000"
                                        }
                                    }
                                },
                                "children": [{
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "Queue Name:"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": true
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 11
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "center"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }, {
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 15]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "        {{scripter.queueName}}"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 10
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }
                                ]
                            }, {
                                "type": "horizontalStackContainer",
                                "properties": {
                                    "margin": {
                                        "typeName": "spacing",
                                        "value": 0
                                    },
                                    "alignment": {
                                        "typeName": "alignment",
                                        "value": "start"
                                    },
                                    "width": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "stretch",
                                            "size": 100
                                        }
                                    },
                                    "height": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "auto"
                                        }
                                    },
                                    "visible": {
                                        "typeName": "variable"
                                    },
                                    "backgroundColor": {
                                        "typeName": "text",
                                        "value": ""
                                    },
                                    "padding": {
                                        "typeName": "spacing",
                                        "value": 10
                                    },
                                    "childArrangement": {
                                        "typeName": "childArrangement",
                                        "value": "start"
                                    },
                                    "border": {
                                        "typeName": "border",
                                        "value": {
                                            "width": 0,
                                            "style": "solid",
                                            "color": "000000"
                                        }
                                    }
                                },
                                "children": [{
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "Interaction State:"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": true
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 11
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }, {
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "    {{scripter.interactionState}}"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 10
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }
                                ]
                            }, {
                                "type": "horizontalStackContainer",
                                "properties": {
                                    "margin": {
                                        "typeName": "spacing",
                                        "value": 0
                                    },
                                    "alignment": {
                                        "typeName": "alignment",
                                        "value": "start"
                                    },
                                    "width": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "stretch",
                                            "size": 100
                                        }
                                    },
                                    "height": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "auto"
                                        }
                                    },
                                    "visible": {
                                        "typeName": "variable"
                                    },
                                    "backgroundColor": {
                                        "typeName": "text",
                                        "value": ""
                                    },
                                    "padding": {
                                        "typeName": "spacing",
                                        "value": 10
                                    },
                                    "childArrangement": {
                                        "typeName": "childArrangement",
                                        "value": "start"
                                    },
                                    "border": {
                                        "typeName": "border",
                                        "value": {
                                            "width": 0,
                                            "style": "solid",
                                            "color": "000000"
                                        }
                                    }
                                },
                                "children": [{
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "Customer ID:"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": true
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 11
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }, {
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "           {{scripter.agentCommunicationId}}"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 10
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }
                                ]
                            }, {
                                "type": "horizontalStackContainer",
                                "properties": {
                                    "margin": {
                                        "typeName": "spacing",
                                        "value": 0
                                    },
                                    "alignment": {
                                        "typeName": "alignment",
                                        "value": "start"
                                    },
                                    "width": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "stretch",
                                            "size": 100
                                        }
                                    },
                                    "height": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "auto"
                                        }
                                    },
                                    "visible": {
                                        "typeName": "variable"
                                    },
                                    "backgroundColor": {
                                        "typeName": "text",
                                        "value": ""
                                    },
                                    "padding": {
                                        "typeName": "spacing",
                                        "value": 10
                                    },
                                    "childArrangement": {
                                        "typeName": "childArrangement",
                                        "value": "start"
                                    },
                                    "border": {
                                        "typeName": "border",
                                        "value": {
                                            "width": 0,
                                            "style": "solid",
                                            "color": "000000"
                                        }
                                    }
                                },
                                "children": [{
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "Customer Number:"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": true
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 11
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }, {
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "{{scripter.agentsFormattedNumber}}"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 10
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }
                                ]
                            }, {
                                "type": "horizontalStackContainer",
                                "properties": {
                                    "margin": {
                                        "typeName": "spacing",
                                        "value": 0
                                    },
                                    "alignment": {
                                        "typeName": "alignment",
                                        "value": "start"
                                    },
                                    "width": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "stretch",
                                            "size": 100
                                        }
                                    },
                                    "height": {
                                        "typeName": "sizing",
                                        "value": {
                                            "sizeType": "auto"
                                        }
                                    },
                                    "visible": {
                                        "typeName": "variable"
                                    },
                                    "backgroundColor": {
                                        "typeName": "text",
                                        "value": ""
                                    },
                                    "padding": {
                                        "typeName": "spacing",
                                        "value": 10
                                    },
                                    "childArrangement": {
                                        "typeName": "childArrangement",
                                        "value": "start"
                                    },
                                    "border": {
                                        "typeName": "border",
                                        "value": {
                                            "width": 0,
                                            "style": "solid",
                                            "color": "000000"
                                        }
                                    }
                                },
                                "children": [{
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "Interaction Type:"
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": true
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 11
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }, {
                                        "type": "text",
                                        "properties": {
                                            "margin": {
                                                "typeName": "spacing",
                                                "value": [5, 5, 5, 5]
                                            },
                                            "alignment": {
                                                "typeName": "alignment",
                                                "value": "start"
                                            },
                                            "width": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "height": {
                                                "typeName": "sizing",
                                                "value": {
                                                    "sizeType": "auto"
                                                }
                                            },
                                            "visible": {
                                                "typeName": "variable"
                                            },
                                            "text": {
                                                "typeName": "interpolatedText",
                                                "value": "     {{scripter.interactionType}}   "
                                            },
                                            "bold": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "italic": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "underline": {
                                                "typeName": "boolean",
                                                "value": false
                                            },
                                            "font": {
                                                "typeName": "font",
                                                "value": "Arial, \"Helvetica Neue\", Helvetica, sans-serif"
                                            },
                                            "fontSize": {
                                                "typeName": "integer",
                                                "value": 10
                                            },
                                            "justification": {
                                                "typeName": "text",
                                                "value": "left"
                                            },
                                            "backgroundColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "textColor": {
                                                "typeName": "text",
                                                "value": ""
                                            },
                                            "padding": {
                                                "typeName": "spacing",
                                                "value": 0
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    }, {
                        "type": "webPage",
                        "properties": {
                            "margin": {
                                "typeName": "spacing",
                                "value": [5, 5, 5, 5]
                            },
                            "alignment": {
                                "typeName": "alignment",
                                "value": "start"
                            },
                            "width": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "pixels",
                                    "size": 480
                                }
                            },
                            "height": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "pixels",
                                    "size": 360
                                }
                            },
                            "visible": {
                                "typeName": "variable"
                            },
                            "webPageSource": {
                                "typeName": "interpolatedText",
                                "value": "http://localhost:3000/CallBack.html?conversationId={{scripter.interactionId}}&participantId={{scripter.participantId}}&environment=mypurecloud.com&clientId=d4dfa94d-d68a-417f-800c-baf332b178c1"
                            }
                        }
                    }, {
                        "type": "verticalStackContainer",
                        "properties": {
                            "margin": {
                                "typeName": "spacing",
                                "value": 0
                            },
                            "alignment": {
                                "typeName": "alignment",
                                "value": "start"
                            },
                            "width": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "stretch",
                                    "size": 100
                                }
                            },
                            "height": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "auto"
                                }
                            },
                            "visible": {
                                "typeName": "variable"
                            },
                            "backgroundColor": {
                                "typeName": "text",
                                "value": ""
                            },
                            "padding": {
                                "typeName": "spacing",
                                "value": 10
                            },
                            "childArrangement": {
                                "typeName": "childArrangement",
                                "value": "start"
                            },
                            "border": {
                                "typeName": "border",
                                "value": {
                                    "width": 0,
                                    "style": "solid",
                                    "color": "000000"
                                }
                            }
                        },
                        "children": []
                    }, {
                        "type": "verticalStackContainer",
                        "properties": {
                            "margin": {
                                "typeName": "spacing",
                                "value": 0
                            },
                            "alignment": {
                                "typeName": "alignment",
                                "value": "start"
                            },
                            "width": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "stretch",
                                    "size": 100
                                }
                            },
                            "height": {
                                "typeName": "sizing",
                                "value": {
                                    "sizeType": "auto"
                                }
                            },
                            "visible": {
                                "typeName": "variable"
                            },
                            "backgroundColor": {
                                "typeName": "text",
                                "value": ""
                            },
                            "padding": {
                                "typeName": "spacing",
                                "value": 10
                            },
                            "childArrangement": {
                                "typeName": "childArrangement",
                                "value": "start"
                            },
                            "border": {
                                "typeName": "border",
                                "value": {
                                    "width": 0,
                                    "style": "solid",
                                    "color": "000000"
                                }
                            }
                        },
                        "children": []
                    }
                ]
            }
        }
    ],
    "features": {
        "dialer": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": true
                },
                "contactListId": {
                    "typeName": "text",
                    "value": "c899ccfc-bcf2-4317-9a2a-3988b1ac23c4"
                },
                "onContactDataLoadAction": {
                    "typeName": "action",
                    "value": {
                        "actionArgumentValues": []
                    }
                }
            }
        },
        "scripter": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": true
                }
            }
        },
        "screenPop": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": false
                }
            }
        },
        "callback": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": false
                }
            }
        },
        "chat": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": false
                }
            }
        },
        "email": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": false
                }
            }
        },
        "message": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": false
                }
            }
        },
        "bridge": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": false
                }
            }
        },
        "uuiData": {
            "properties": {
                "enabled": {
                    "typeName": "boolean",
                    "value": false
                }
            }
        }
    },
    "variables": [],
    "customActions": [],
    "dataVersion": 0
}

export const CallBackScript = jsonObj;