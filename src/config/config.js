//const 
const development = {

    // PureCloud assigned name for the premium app
    // This should match the integration type name of the Premium App
    integrationType: "premium-app-autocallback",

    // Default Values for fail-safe/testing. Shouldn't have to be changed since the app
    // must be able to determine the environment from the query parameter 
    // of the integration's URL
    defaultPcEnv: "mypurecloud.com",
    defaultLangTag: "en-us",

    // Permissions required for running the Wizard App
    setupPermissionsRequired: ['admin'],

    // Application name
    appName: "Automatic Callback Dev",

    // URL of the application
    //appUrl: "http://localhost:3000/",
    appUrl: "https://d3i55tcmvnovry.cloudfront.net/",

    // clientId of the application
    clientId: 'd4dfa94d-d68a-417f-800c-baf332b178c1',
    //clientId: "49fae925-0757-4dc2-8802-a9f20df95e19", // US WEST    

    // These are the PureCloud items that will be added and provisioned by the wizard
    provisioningInfo: {
        "role": 
            {
                "name": "Role",
                "description": "Generated role for access to the script.",
                "permissionPolicies": [
                    {
                        "domain": "integration",
                        "entityName": "automaticCallback",
                        "actionSet": ["view"],
                        "allowConditions": false
                    },
                    {
                        "domain": "integrations",
                        "entityName": "integration",
                        "actionSet": ["view"],
                        "allowConditions": false
                    }
                ]
            }        
    }
};

const test = {

    // PureCloud assigned name for the premium app
    // This should match the integration type name of the Premium App
    integrationType: "premium-app-autocallback",

    // Default Values for fail-safe/testing. Shouldn't have to be changed since the app
    // must be able to determine the environment from the query parameter 
    // of the integration's URL
    defaultPcEnv: "mypurecloud.com",
    defaultLangTag: "en-us",

    // Permissions required for running the Wizard App
    setupPermissionsRequired: ['admin'],

    // Application name
    appName: "Automatic Callback Test",

    // URL of the application
    appUrl: "https://dmdnuh3ssl7z0.cloudfront.net/",

    // clientId of the application
    clientId: '49fae925-0757-4dc2-8802-a9f20df95e19',

    // These are the PureCloud items that will be added and provisioned by the wizard
    provisioningInfo: {
        "role": 
            {
                "name": "Role",
                "description": "Generated role for access to the script.",
                "permissionPolicies": [
                    {
                        "domain": "integration",
                        "entityName": "automaticCallback",
                        "actionSet": ["view"],
                        "allowConditions": false
                    },
                    {
                        "domain": "integrations",
                        "entityName": "integration",
                        "actionSet": ["view"],
                        "allowConditions": false
                    }
                ]
            }        
    }
};

const production = {
    // PureCloud assigned name for the premium app
    // This should match the integration type name of the Premium App
    integrationType: "premium-app-autocallback",

    // Default Values for fail-safe/testing. Shouldn't have to be changed since the app
    // must be able to determine the environment from the query parameter 
    // of the integration's URL
    defaultPcEnv: "mypurecloud.com",
    defaultLangTag: "en-us",

    // Permissions required for running the Wizard App
    setupPermissionsRequired: ['admin'],

    // Application name
    appName: "Automatic Callback",

    // URL of the application
    appUrl: "https://automaticcallback.genesyscsp.com/",

    // clientId of the application
    clientId: "49fae925-0757-4dc2-8802-a9f20df95e19", // US WEST    

    // These are the PureCloud items that will be added and provisioned by the wizard
    provisioningInfo: {
        "role": 
            {
                "name": "Role",
                "description": "Generated role for access to the script.",
                "permissionPolicies": [
                    {
                        "domain": "integration",
                        "entityName": "automaticCallback",
                        "actionSet": ["view"],
                        "allowConditions": false
                    },
                    {
                        "domain": "integrations",
                        "entityName": "integration",
                        "actionSet": ["view"],
                        "allowConditions": false
                    }
                ]
            }        
    }
};

const env = process.env.REACT_APP_CUSTOM_ENV.trim();
console.log('Loading ' + env + ' configuration');

var conf = development;

if(env === 'production')
{
    conf = production;
}
else if(env === 'test')
{
    conf = test;
}

export const configuration = conf;