import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

import App from './App';

const jsx = (
    <Router>
        <App />
    </Router>
)

ReactDOM.render(jsx, document.getElementById('root'));
registerServiceWorker();