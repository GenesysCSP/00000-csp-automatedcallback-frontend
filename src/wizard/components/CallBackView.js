import React, { Component } from 'react';
import CallBack from '../scripts/CallBack';

class CallBackView extends Component {
    componentDidMount() {
        let CallBackApp = new CallBack();
        CallBackApp.StartCallBack();
    };

    componentDidUnMount() {
        console.log('Callback view Unmounted')
    }

    render() {
        return (
            <div>
            </div>
        )
    }
};

export default CallBackView;