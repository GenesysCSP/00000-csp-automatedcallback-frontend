import React, { Component } from 'react';

class HelpView extends Component {

    render() {
        return (
            <div className="full-height">
                <div id="background"></div>
                <noscript>
                    For full functionality of this site it is necessary to enable JavaScript. Here are the <a
                        href="http://www.enable-javascript.com/" target="_blank" rel="noopener noreferrer">instructions how to enable JavaScript in your web
                    browser</a>.
                    </noscript>

                <header>
                    <p>Automatic Callback for Genesys Cloud /&nbsp; Help</p>
                </header>
                <div className="wiz-content">
                    <div className="title">
                        Guidelines
                        </div>
                    <main className="help-content">
                        <div style={{ position: "inherit", top: 0, left: 0, right: 0, bottom: 0, margin: "auto", width: "93%", textAlign: "left" }}>
                            <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div className="panel panel-default">
                                    <div className="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 className="panel-title">
                                            <span>Call Back</span>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div className="panel-body">
                                            <div className="row">
                                                <div className="col">
                                                    <p>A callback is a request callers can make to have their call returned when an agent is unavailable to take it right away. In contact centers, callbacks provide assistance for busy agents and provide an extra level of service to customers who encounter wait times.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel panel-default">
                                    <div className="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 className="panel-title">
                                            <span>Setting up of Script</span>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div className="panel-body">
                                            <div className="row">
                                                <div className="col">
                                                    <p>Following are the steps for the creation of the new script that will automate the process of callback.</p>
                                                    <span>
                                                        <ol style={{ marginLeft: "55px" }}>
                                                            <li>Click Admin ->  Scripts, to navigate to the Scripts page</li>
                                                            <li>Click Create button to create a new Script.</li>
                                                            <img src="../images/create.png" alt="create" style={{ width: "70%" }} />
                                                            <li>Enter the name of the script and select "Default Callback Script"</li>
                                                            <img src="../images/name.png" alt="name" />
                                                            <li>On selection, Script Page that will be displayed on the Agent's screen when callback comes in is displayed. By default, Interaction type, Interaction state, Queue Name, Customer's number are added to the page. </li>
                                                            <li>On the right panel, we have the option to add or remove interaction details displayed on the Script page. The various interaction details are available as variables.</li>
                                                            <li>Click and drag the Web Page (Globe icon) option to add the external page to the Script page.</li>
                                                            <li>Click the added web page, In the <b>Common</b> pane, under the <b>Web Page Source</b> enter the URL for the external page. Following will be the URL for the external page <em>https://automaticcallback.genesyscsp.com/CallBack.html?conversationId={"{{"}Scripter.Interaction ID{"}}"}&amp;participantId={"{{"}Scripter.Agent Participant ID{"}}"}&amp;interactionState={"{{"}Scripter.Interaction State{"}}"}&amp;environment={"<"}environemnt of client{">"}</em></li>
                                                            <img src="../images/options.png" alt="options" />
                                                            <li>Click Script -> Script Properties, to open the Properties pane. Enable the Outbound and Callback options.</li>
                                                            <img src="../images/properties.png" alt="properties" />
                                                            <li>Click Script -> Save, to save the script.</li>
                                                            <li>Click Script -> Publish, to publish the script. Once the script is published, the script will be enabled and available to execute.</li>
                                                        </ol>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel panel-default">
                                    <div className="panel-heading" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <h4 className="panel-title">
                                            <span>Execution of Script</span>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div className="panel-body">
                                            <div className="row">
                                                <div className="col">
                                                    <p>The Script can be used in making a callback through API or in an architect flow for a callback. When the script is called, the interaction id and the participant id are passed to script, based on which the callback is answered and the outbound call is placed.</p>
                                                    <p>The Agents should be assigned with the role <strong>Automatic Callback Role</strong> for the script to be executed for the agent.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel panel-default">
                                    <div className="panel-heading" role="tab" id="headingFour" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <h4 className="panel-title">
                                            <span>Implementation of Script</span>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div className="panel-body">
                                            <div className="row">
                                                <div className="col">
                                                    <p>Following are the few methods of implementation of the installed script.</p>
                                                    <span>
                                                        <ul style={{ marginLeft: "55px" }}>
                                                            <li>Triggring Callback through API</li>
                                                            <p> Genesys Cloud API can be used to place a callback to a queue. When the API is called, in the API payload we can provide the scriptId. The provided script will be triggered when the call is placed to an Agent in the queue. Following link gives a detailed information on the payload to be sent for callback.
                                                            <br />
                                                                <a href="https://developer.mypurecloud.com/api/rest/client-libraries/javascript/ConversationsApi.html#postConversationParticipantCallbacks" target="_blank" rel="noopener noreferrer">Callback API</a>
                                                            </p>
                                                            <li>Calling the script in callback flow</li>
                                                            <p>As the installation process is completed successfully, the script will be uploaded and published. All published scripts will be available in the architect. When the callback flow is designed through an architect, the script can be selected during the designing of the flow on the required step.</p>
                                                        </ul>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <footer>
                </footer>
            </div>
        )
    }
};

export default HelpView;