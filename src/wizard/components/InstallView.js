import React, { Component } from 'react';
import '../style.css';
import WizardApp from '../scripts/wizard-app';

let callbackAutomationApp;

class InstalView extends Component {
     componentDidMount() {
        callbackAutomationApp = new WizardApp();
        callbackAutomationApp.start();
    };

    InstallConfiguration = () => {

        callbackAutomationApp.installConfigurations().then(() => {
            this.props.history.push('/finish'); 
        }).catch((err) => {
            this.props.history.push('/finish/' + err); 
        });
    };

    render() {
        return (
            <div className="full-height">
                <div id="background"></div>
                <noscript>
                    For full functionality of this site it is necessary to enable JavaScript. Here are the <a
                        href="http://www.enable-javascript.com/" target="_blank" rel="noopener noreferrer">instructions how to enable JavaScript in your web
                    browser</a>.
                    </noscript>

                <header>
                    <p>Automatic Callback for Genesys Cloud /&nbsp; Install</p>
                </header>
                <div className="wiz-content">
                    <div className="title">
                        Installation
                        </div>

                    <ul className="wiz-progress-bar">
                        <li className="active">
                            <span className="txt-start">Start</span>
                        </li>
                        <li className="active current">
                            <span className="txt-install">Install</span>
                        </li>
                        <li className="">
                            <span className="txt-summary">Summary</span>
                        </li>
                    </ul>

                    <main>
                        <p>
                            <span className="txt-install-summary">These are the steps that will be automatically performed for
                                    you!</span>
                        </p>
                        
                        <div className="message">
                            <div className="message-title">
                                <span className="txt-create-role">1. Create Role</span>
                            </div>
                            <div className="message-content">
                                <div>
                                    <span className="txt-create-role-msg">
                                        Create a role with name "Automatic Callback Role" specifically to provide access to the Automatic Callback application.
                                        </span>
                                </div>
                            </div>
                        </div>

                        <div className="message">
                            <div className="message-title">
                                <span className="txt-create-role">2. Assign role to the user</span>
                            </div>
                            <div className="message-content">
                                <div>
                                    <span className="txt-create-role-msg">
                                        Assigns the created role to the user who does the installation.
                                        </span>
                                </div>
                            </div>
                        </div>

                       {/* <div className="message">
                            <div className="message-title">
                                <span className="txt-create-role">2. Create Callback Script</span>
                            </div>
                            <div className="message-content">
                                <div>
                                    <span className="txt-create-role-msg">
                                        Creates the script for the Automatic callback for Agents.
                                        </span>
                                </div>
                            </div>
                        </div>

                        <div className="message">
                            <div className="message-title">
                                <span className="txt-create-instance">3. Upload Script</span>
                            </div>
                            <div className="message-content">
                                <div>
                                    <span className="txt-create-instance-msg">
                                        Upload the created script to the Genesys Cloud.
                                        </span>
                                </div>
                            </div>
                        </div>

                        <div className="message">
                            <div className="message-title">
                                <span className="txt-create-instance">4. Publish Script</span>
                            </div>
                            <div className="message-content">
                                <div>
                                    <span className="txt-create-instance-msg">
                                        Publish the script to make it available for users.
                                        </span>
                                </div>
                            </div>
                        </div> */}


                        <p className="sink-more">
                            <span className="txt-start-install">
                                Please click the Install button to start the installation.
                                </span>
                        </p>

                        <button id="start" onClick={this.InstallConfiguration.bind(this)} className="wiz-btn-info right">
                            <span className="txt-install">Install</span>
                        </button>
                    </main>
                </div>
                <footer>
                </footer>
            </div>
        )
    }
};

export default InstalView;