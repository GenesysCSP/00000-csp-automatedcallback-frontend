import { configuration as config } from '../../config/config'
import { sleep } from '../../wizard/scripts/util'
const appConfig = config;

const platformClient = window.require('platformClient');
const client = platformClient.ApiClient.instance;
const conversationsApi = new platformClient.ConversationsApi();
const notificationsApi = new platformClient.NotificationsApi();
const userApi = new platformClient.UsersApi();
const integrationsApi = new platformClient.IntegrationsApi();

const pageSize = 500;
const integrationType = "premium-app-autocallback";
//const integrationType = "premium-app-example";
const redirectUri = window.location.origin + window.location.pathname;
let userConversationsTopic = '', conversationId = "", participantId = "", callbackNumber = "", installationData, userId = ""

class CallBack {
    constructor() {
        console.log('**** - Script loads', (new Date()).toLocaleString())

        installationData = appConfig.provisioningInfo;
        let params = new URLSearchParams(window.location.search);
        this.clientId = appConfig.clientId; //YOUR CLIENT ID HERE
        this.environment = params.get("environment");
        userConversationsTopic = '';
        conversationId = params.get("conversationId");
        participantId = params.get("participantId");
        this.interactionState = params.get("interactionState");

        if (this.environment) {
            sessionStorage.setItem('AutomaticCallback:environment', this.environment)
        } else {
            this.environment = sessionStorage.getItem('AutomaticCallback:environment')
        }

        if (conversationId) {
            sessionStorage.setItem('AutomaticCallback:conversationId', conversationId)
        } else {
            conversationId = sessionStorage.getItem('AutomaticCallback:conversationId')
            sessionStorage.removeItem('AutomaticCallback:conversationId')
        }

        if (participantId) {
            sessionStorage.setItem('AutomaticCallback:participantId', participantId)
        } else {
            participantId = sessionStorage.getItem('AutomaticCallback:participantId')
            sessionStorage.removeItem('AutomaticCallback:participantId')
        }

        // Set PureCloud settings
        client.setEnvironment(this.environment);
        client.setPersistSettings(true, 'AutomaticCallback');

        console.log('**** - ', (new Date()).toLocaleString(), ' - conversation Id:', conversationId)
        console.log('**** - participant Id:', participantId)
        console.log('**** - ', redirectUri)

    }

    async StartCallBack() {
        console.log('**** - Script starts', (new Date()).toLocaleString())
        console.log('**** - Interaction State', this.interactionState)
        try {
            // Authenticate with Genesys Cloud
            await client.loginImplicitGrant(this.clientId, redirectUri)

            if (sessionStorage.getItem('AutomaticCallback:isEntityEnabled') === null || sessionStorage.getItem('AutomaticCallback:isEntityEnabled') === "false") {
                console.log('**** - Querying to check if org is enabled for automatic callback');
                let entities = await this.getEntities(integrationsApi, 'getIntegrationsTypes')
                let isEntityEnabled = entities.filter((integType) => integType.id === integrationType)[0]

                if(isEntityEnabled === undefined) {
                    console.log(`Checking for PS version app - ${appConfig.appName}`)
                    entities = await this.getEntities(integrationsApi, 'getIntegrations')
                    isEntityEnabled = entities.filter((integType) => integType.name === appConfig.appName)[0]
                }
                console.log(`Is App installed and enabled: ${isEntityEnabled}`)

                sessionStorage.setItem('AutomaticCallback:isEntityEnabled', isEntityEnabled !== undefined)
            }

            if (sessionStorage.getItem('AutomaticCallback:isEntityEnabled') === "false")
                throw new Error("Organization is not authorized for automatic callback");
            else
                console.log('**** - Organization is authorized for automatic callback');

            let opts = {
                'expand': ["authorization"] // [String] | Which fields, if any, to expand.
            };

            // Get Id of user
            let me = await userApi.getUsersMe(opts);
            userId = me.id

            //Validate if role is enabled for User
            if (sessionStorage.getItem('AutomaticCallback:isRoleEnabled') === null || sessionStorage.getItem('AutomaticCallback:isRoleEnabled') === "false") {
                let isRoleEnabled = me.authorization.roles.filter(x => x.name === (appConfig.appName + ' ' + installationData.role.name))
                sessionStorage.setItem('AutomaticCallback:isRoleEnabled', isRoleEnabled.length > 0)
            }

            if (sessionStorage.getItem('AutomaticCallback:isRoleEnabled') === "false")
                throw new Error("Agent is not authorized for automatic callback");
            else
                console.log('**** - Agent is authorized for automatic callback');

            //Main logic starts
            if (this.interactionState === null || this.interactionState === "Connected") {

                if (this.interactionState === null) {
                    sessionStorage.removeItem('AutomaticCallback:channelId')
                }

                var currentCall = await conversationsApi.getConversationsCall(conversationId)
                console.log('**** - ', (new Date()).toLocaleString(), "Current Call: ", currentCall)

                let participant = currentCall.participants.slice().reverse().find(p => p.purpose === "agent" && p.id === participantId)
                console.log('**** - ', (new Date()).toLocaleString(), "Current Participant: ", participant)

                //Interaction already dialed before loading
                if (participant && participant.state === 'connected') {
                    console.log('**** - ', (new Date()).toLocaleString(), "Call already dialed")
                    await this.createWebSocketChannel(userId)
                }

                //Interaction already disconnected
                if (participant && participant.state !== 'connected') {
                    console.log('**** - ', (new Date()).toLocaleString(), "'Call ended. Disconnecting Callback")
                    // Set body for next API call to disconnect callback
                    let body = {
                        "state": "disconnected"
                    };

                    // API call to end callback
                    conversationsApi.patchConversationsCallbackParticipant(conversationId, participantId, body)
                        .then(() => console.log('**** - ', (new Date()).toLocaleString(), ' - Disconnecting - patchConversationsCallbackParticipant returned successfully.'))
                        .catch((err) => console.log(`${new Date().toLocaleTimeString()} - ${JSON.stringify(err, null, 2)}`));

                    // Clear conversationId to avoid repetitive API traffic
                    conversationId = "";
                }

                //Dialing Callback
                if (!participant) {

                    if (this.interactionState === null) {
                        // Set body for next API to connect callback to agent
                        let body = {
                            "state": "connected"
                        };
                        console.log('**** - ', (new Date()).toLocaleString(), "Answering call when interaction state is null")
                        //API call to answer callback
                        conversationsApi.patchConversationsCallbackParticipant(conversationId, participantId, body)
                    }

                    // Get callback data
                    console.log('**** - ', (new Date()).toLocaleString(), " - Getting Callback data")
                    let data = await conversationsApi.getConversationsCallback(conversationId);

                    // Extract the callback number from the callback object
                    callbackNumber = data.participants.filter(participant => participant.purpose === "agent")[0].callbackNumbers[0];
                    console.log('**** - ', (new Date()).toLocaleString(), "Callback number", callbackNumber)

                    // Set body for next API call to place outbound call
                    console.log('**** - ', (new Date()).toLocaleString(), " - Dialing callback")

                    let body = {
                        "callNumber": callbackNumber
                    };

                    // Place outbound call
                    conversationsApi.postConversationsCall(conversationId, body)
                        .then(() => console.log('**** - ', (new Date()).toLocaleString(), ' - Connecting - patchConversationsCallbackParticipant returned successfully.'))
                        .catch((err) => console.log(`${new Date().toLocaleTimeString()} -  Placing call failed - ${JSON.stringify(err, null, 2)}`));

                    await this.createWebSocketChannel(userId)
                }
            }

            if (this.interactionState === "Disconnected") {
                console.log('**** - ', (new Date()).toLocaleString(), "'Call ended. Disconnecting Callback")
                // Set body for next API call to disconnect callback
                let body = {
                    "state": "disconnected"
                };

                // API call to end callback
                conversationsApi.patchConversationsCallbackParticipant(conversationId, participantId, body)
                    .then(() => console.log('**** - ', (new Date()).toLocaleString(), ' - Disconnecting - patchConversationsCallbackParticipant returned successfully.'))
                    .catch((err) => console.log(`${new Date().toLocaleTimeString()} - ${JSON.stringify(err, null, 2)}`));

                // Clear conversationId to avoid repetitive API traffic
                conversationId = "";
            }

        } catch (err) {
            if (err.status === 429) {
                console.log(`Waiting ${err.retryAfterMs / 1000} seconds before reaching to Genesys Cloud`)
                await sleep(err.retryAfterMs)
                console.log("Calling again after 429 error")
                this.StartCallBack()
            } else console.error(err)
        }
    }

    async createWebSocketChannel(userId) {

        if (sessionStorage.getItem("AutomaticCallback:channelId") === null || (new Date(sessionStorage.getItem("AutomaticCallback:channelIdExpiry")).setHours(0, 0, 0, 0) <= (new Date().setHours(0, 0, 0, 0)))) {
            console.log('**** - ', (new Date()).toLocaleString(), " - Creating notification channel")
            let notificationChannel = await notificationsApi.postNotificationsChannels();
            console.log('**** - ', (new Date()).toLocaleString(), ' - Channel: ', notificationChannel);
            sessionStorage.setItem("AutomaticCallback:channelId", notificationChannel.id)
            sessionStorage.setItem("AutomaticCallback:channelIdExpiry", notificationChannel.expires)
        } else {
            console.log('**** - ', (new Date()).toLocaleString(), " - Getting channel id from session", sessionStorage.getItem("AutomaticCallback:channelId"))
        }

        // Activate notifications stream
        console.log('**** - ', (new Date()).toLocaleString(), " - Activate notifications stream for user", userId)
        userConversationsTopic = 'v2.users.' + userId + '.conversations';
        notificationsApi.putNotificationsChannelSubscriptions(sessionStorage.getItem("AutomaticCallback:channelId"), [{ id: userConversationsTopic }]);

        console.log('**** - ', (new Date()).toLocaleString(), " - Channel URL", `wss://streaming.${this.environment}/channels/${sessionStorage.getItem("AutomaticCallback:channelId")}`)

        // Set up web socket
        let webSocket = new WebSocket(`wss://streaming.${this.environment}/channels/${sessionStorage.getItem("AutomaticCallback:channelId")}`);
        webSocket.onmessage = this.handleNotification;

    }


    // Handle incoming Genesys Cloud notification from WebSocket
    handleNotification(message) {
        // Parse notification string to a JSON object
        let notification = JSON.parse(message.data);
        console.log('**** - ', (new Date()).toLocaleString(), " - WSS event - ", notification);
        // Discard unwanted notifications
        if (notification.topicName.toLowerCase() === 'channel.metadata') {
            // Heartbeat
            return;
        } else if (notification.topicName.toLowerCase() !== userConversationsTopic.toLowerCase()) {
            // Unexpected topic
            return;
        }

        let participant = notification.eventBody.participants.filter(x => x.purpose === "agent").filter(x => x.userId === userId).reverse()[0];

        // Test notification event to see if it is for the callback call termination
        if (participant && participant.calls && participant.calls[0].state === "terminated" && conversationId === notification.eventBody.id) {

            console.log('**** - ', (new Date()).toLocaleString(), " - disconnect callback");
            //sessionStorage.removeItem('AutomaticCallback:isCallbackDialed')

            // Set body for next API call to disconnect callback
            let body = {
                "state": "disconnected"
            };

            // API call to end callback
            conversationsApi.patchConversationsCallbackParticipant(conversationId, participantId, body)
                .then(() => console.log('**** - ', (new Date()).toLocaleString(), ' - Disconnecting - patchConversationsCallbackParticipant returned successfully.'))
                .catch((err) => console.log(`${new Date().toLocaleTimeString()} - ${JSON.stringify(err, null, 2)}`));

            // Clear conversationId to avoid repetitive API traffic
            conversationId = "";
        }

    }

    getEntities(api, func, options) {
        const entities = [];
        let pageNumber = 1;

        const getEntitiesWorker = () => {
            return api[func]({ ...options, pageSize, pageNumber })
                .then((data) => {
                    console.log(`${func}, pageSize = ${pageSize}, pageNumber = ${pageNumber}, entities = ${data.entities.length}`);
                    entities.push(...data.entities);
                    return (pageNumber++ < data.pageCount) ? getEntitiesWorker() : entities;
                });
        }
        return getEntitiesWorker();
    }
}

export default CallBack;