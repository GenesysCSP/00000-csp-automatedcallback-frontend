/*
*   NOTE: This sample uses ES6 features
*/
import { configuration as config } from '../../config/config'
import { CallBackScript } from '../../config/CallBackScript'
const platformClient = window.require('platformClient');

const appConfig = config;

// JQuery Alias
const $ = window.$;

// Relative path to wizard page from config's redirectUri
//const WIZARD_PAGE = "/index.html";

const pageSize = 500;
/**
 * WizardApp class that handles everything in the App.
 */
class WizardApp {
    constructor(redirectUri) {
        this.pcEnvironment = null;

        // PureCloud Javascript SDK clients
        //this.platformClient = require('platformClient');
        this.platformClient = platformClient;
        this.purecloudClient = this.platformClient.ApiClient.instance;
        this.purecloudClient.setPersistSettings(true, appConfig.integrationType);
        this.redirectUri = appConfig.appUrl;

        // console.log('WizardApp constructor, redirectUri = ' + this.redirectUri);

        // PureCloud API instances
        this.usersApi = new this.platformClient.UsersApi();
        this.integrationsApi = new this.platformClient.IntegrationsApi();
        this.authApi = new this.platformClient.AuthorizationApi();
        this.oAuthApi = new this.platformClient.OAuthApi();
        this.orgApi = new this.platformClient.OrganizationApi();
        this.scriptsApi = new platformClient.ScriptsApi();

        // Language default is english
        // Language context is object containing the translations
        this.language = appConfig.defaultLangTag;

        this.integrationType = appConfig.integrationType;

        this.appName = appConfig.appName;
        this.appUrl = appConfig.appUrl;

        this.installationData = appConfig.provisioningInfo;
    }

    getEntities(api, func, options) {
        const entities = [];
        let pageNumber = 1;

        const getEntitiesWorker = () => {
            return api[func]({ ...options, pageSize, pageNumber })
                .then((data) => {
                    console.log(`${func}, pageSize = ${pageSize}, pageNumber = ${pageNumber}, entities = ${data.entities.length}`);
                    entities.push(...data.entities);
                    return (pageNumber++ < data.pageCount) ? getEntitiesWorker() : entities;
                });
        }
        return getEntitiesWorker();
    }

    /**
     * Get details of the current user
     * @return {Promise.<Object>} PureCloud User data
     */
    getUserDetails() {
        const opts = { 'expand': ['authorization'] };
        return this.usersApi.getUsersMe(opts);
    }

    /**
     * Checks if the product is available in the current Purecloud org.
     * @return {Promise.<Boolean>}
     */
    validateProductAvailability() {
        // premium-app-autocallback         
        return this.getEntities(this.integrationsApi, 'getIntegrationsTypes')
            .then((entities) => {
                // console.log('Integration types: ' + JSON.stringify(entities, null, 3));

                if (entities.filter((integType) => integType.id === this.integrationType)[0]) {
                    console.log("PRODUCT AVAILABLE");
                    return (true);
                } else {
                    console.log("PRODUCT NOT AVAILABLE");
                    return (false);
                }
            });
    }

    /**
     * Checks if any configured objects are still existing. 
     * This is based on the appName
     * @returns {Promise.<Boolean>} If any installed objects are still existing in the org. 
     */
    isExisting() {
        const promiseArr = [];

        promiseArr.push(this.getExistingRoles());
        //promiseArr.push(this.getExistingScripts());

        return Promise.all(promiseArr).then((results) => {
            console.log('isExisting, results = ' + JSON.stringify(results, null, 3));
            console.log(results[0].length)
            return results[0].length;
        });
    }


    //// =======================================================
    ////      ROLES
    //// =======================================================

    /**
     * Get existing roles in purecloud based on appName
     * @returns {Promise.<Array>} PureCloud Roles
     */
    getExistingRoles() {
        const authOpts = {
            'name': this.appName.replace(/\(/g, '\\(').replace(/\)/g, '\\)') + '*', // Wildcard to work like STARTS_WITH, need to escape ) and (
            'userCount': false
        };

        // console.log('Get existing roles: ' + JSON.stringify(authOpts, null, 3));

        return this.getEntities(this.authApi, 'getAuthorizationRoles', authOpts);
    }

    /**
     * Get existing scripts in purecloud
     * @returns {Promise.<Array>} PureCloud Roles
     */
    async getExistingScripts() {

        let orgDetails = await this.orgApi.getOrganizationsMe()

        let opts = { 
            'pageSize': 25, // Number | Page size
            'pageNumber': 1, // Number | Page number
            'name': "CallBackScript_" + orgDetails.name // String | Name filter           
          };

        // console.log('Get existing roles: ' + JSON.stringify(authOpts, null, 3));

        return this.getEntities(this.scriptsApi, 'getScripts', opts);
    }

    /**
     * Add PureCLoud role based on installation data
     * @returns {Promise}
     */
    addRole() {
        console.log('Adding role');
        const roleName = this.appName + ' ' + this.installationData.role.name;
        this.logInfo('Creating role ' + roleName);

        const roleBody = {
            name: roleName,
            description: this.installationData.role.description,
            permissionPolicies: this.installationData.role.permissionPolicies
        };

        let role;

        // Create the role
        return this.authApi.postAuthorizationRoles(roleBody)
            .then((data) => {
                this.logInfo('Created role ' + roleName);
                role = data;
                return this.getUserDetails();
            })
            .then((user) => {
                // Assign the role to the user
                // Required before you can assign the role to an Auth Client.
                return this.authApi.putAuthorizationRoleUsersAdd(role.id, [user.id]);
            })
            .then(() => {
                this.logInfo('Assigned role ' + roleName + ' to user.');
                return role;
            })
            .catch((err) => {
                console.log('Role creation error:' + JSON.stringify(err, null, 3));
                throw err;
            });
    }

    //// =======================================================
    ////      OAUTH2 CLIENT
    //// =======================================================

    /**
     * Get existing authetication clients based on the prefix
     * @returns {Promise.<Array>} Array of PureCloud OAuth Clients
     */
    getExistingAuthClients() {
        return this.getEntities(this.oAuthApi, 'getOauthClients')
            .then((entities) => {
                //console.log('getExistingAuthClients, entities = ' + JSON.stringify(entities, null, 3));
                return entities.filter(entity => entity.name && entity.name.startsWith(this.appName));
            });
    }

    /**
     * Add PureCLoud instance based on installation data
     * @returns {Promise.<Array>} PureCloud OAuth objects
     */
    addAuthClient(role) {
        console.log('Adding oauth client');
        // console.log('role = ' + JSON.stringify(role, null, 3));

        const oauthName = this.appName + ' ' + this.installationData.oauth.name;

        const oauthClient = {
            name: oauthName,
            description: this.installationData.oauth.description,
            roleIds: [role.id],
            authorizedGrantType: "CLIENT_CREDENTIALS"
        };

        return this.oAuthApi.postOauthClients(oauthClient)
            .then((data) => {
                this.logInfo(`Created OAuth client "${oauthName}"`);
                return data;
            })
            .catch((err) => {
                console.log('Oauth creation error:' + JSON.stringify(err, null, 3));
                throw err;
            })
    }

    //// =======================================================
    ////      ORGANIZATION
    //// =======================================================

    updateAppInstance() {
        console.log('Update "%s", url = %s', this.appName, this.appUrl);

        return this.getAppInstance()
            .then(integration => {
                // console.log('App instance: ' + JSON.stringify(integration, null, 3));

                return Promise.all([integration.id, this.integrationsApi.getIntegrationConfigCurrent(integration.id)]);
            })
            .then(values => {
                console.log('App instance current config: ' + JSON.stringify(values[1], null, 3));

                const integrationsOpts = {
                    body: values[1]  // integration config current
                };
                integrationsOpts.body.properties.url = this.appUrl;

                console.log('App instance new config: ' + JSON.stringify(integrationsOpts, null, 3));

                return this.integrationsApi.putIntegrationConfigCurrent(values[0], integrationsOpts);
            });
    }

    //// =======================================================
    ////      PROVISIONING
    //// =======================================================

    /**
     * Final Step of the installation wizard. 
     * Create the PureCloud objects defined in provisioning configuration
     * The order is important for some of the PureCloud entities.
     */
    installConfigurations() {

        console.log('Installing configurations');

        $.LoadingOverlay("show", {
            image: "",
            fontawesomeResizeFactor: 2.5,
            fontawesome: "fa fa-spinner fa-spin",
            fontawesomeColor: '#DDDDDD',
            fontawesomeOrder: 1,
            text: "Configuring your App",
            textResizeFactor: 0.17,
            textOrder: 2,
            textClass: "adjust-text",
            background: "rgba(240, 240, 255, 0.9)"
        });


        return this.getAppInstance()
            // Create Role
            .then(() => this.addRole())

            // Create Group
            //.then(() => this.addGroup())

            // Create Integration for widget
            //.then(() => this.addIntegration())

            // Create Integration for widget
            //.then((integrationData) => this.updateIntegration(integrationData))

            //.then(() => this.orgApi.getOrganizationsMe())

            //.then((orgDetails) => this.createScriptInstance(orgDetails))

            /*.then((scriptUploadData) => {
                this.logInfo('Created callback script');

                let opts = {
                    'longPoll': false // Boolean | Enable longPolling endpoint
                };

                this.logInfo('Uploading callback script');
                return this.scriptsApi.getScriptsUploadStatus(scriptUploadData.correlationId, opts)
                    .then((uploadStatus) => {
                        console.log(`getScriptsUploadStatus success! data: ${JSON.stringify(uploadStatus, null, 2)}`);
                        console.log(uploadStatus)

                        if (uploadStatus.succeeded) {
                            let opts = {
                                'pageSize': 25, // Number | Page size
                                'pageNumber': 1, // Number | Page number
                                'name': CallBackScript.name, // String | Name filter
                            };

                            return this.getEntities(this.scriptsApi, 'getScripts', opts);
                        } else {
                            throw new Error(`Error in uploading the script.`)
                        }
                    })
            })

            .then((uploadedScript) => {
                console.log(`Uploaded Script details`, uploadedScript)
                this.logInfo('Uploaded callback script');

                var ourScript = uploadedScript.find(x => x.name === CallBackScript.name)

                if (ourScript !== undefined) {
                    this.logInfo('Publishing callback script');
                    return new Promise((resolve, reject) => {
                        $.ajax({
                            type: 'POST',
                            headers: {
                                Authorization: 'bearer ' + localStorage.getItem(this.integrationType + ':accessToken')
                            },
                            url: 'https://apps.' + localStorage.getItem(this.integrationType + ":environment") + '/platform/api/v2/scripts/published?scriptDataVersion=0&pageDataVersion=0',
                            contentType: 'application/json',
                            dataType: 'json',
                            data: JSON.stringify({ "scriptId": ourScript.id }),
                            success: resolve,
                            error: function (xhr, status, error) {
                                console.log('Error in publish script, xhr = ' + JSON.stringify(xhr, null, 3));
                                console.log('Error in publish script, status = ' + JSON.stringify(status, null, 3));
                                console.log('Error in publish script, error = ' + JSON.stringify(error, null, 3));

                                reject("Error in publishing the uploaded script.");
                            }
                        });
                    });
                } else {
                    throw new Error(`Unable to publish the uploaded script. Please contact administrator.`)
                }
            })*/

            // When everything's finished, log a success message.
            .then(() => {
                this.logInfo('Installation Complete!');
                setTimeout(function () { $.LoadingOverlay("hide"); }, 1500);
            }).catch((err) => {
                this.logInfo('Error occured!');
                setTimeout(function () { $.LoadingOverlay("hide"); }, 1500);
                throw err;
            });
    }

    getAppInstance() {
        return this.getEntities(this.integrationsApi, 'getIntegrations')
            .then(entities => {
                // console.log('Integrations: ' + JSON.stringify(entities, null, 3));

                //return (entities.find(entity => entity.integrationType.id === this.integrationType && entity.name === this.appName));
                return (entities.find(entity => entity.integrationType.id === this.integrationType));
            })
            .then(integration => {
                if (!(integration && integration.id)) {
                    throw new Error(`Could not find integration - "${this.appName}".`);
                }
                return integration;
            });
    }

    createScriptInstance(orgDetails) {
        this.logInfo('Creating callback script');
        console.log(orgDetails)
        //Update URI
        CallBackScript.name = "CallBackScript_" + orgDetails.name;
        CallBackScript.pages[0].rootContainer.children.find(x => x.type === "webPage").properties.webPageSource.value = appConfig.appUrl + "CallBack.html?conversationId={{scripter.interactionId}}&participantId={{scripter.participantId}}&environment=" + localStorage.getItem(this.integrationType + ":environment");

        const str = JSON.stringify(CallBackScript);
        const bytes = new TextEncoder().encode(str);
        const blob1 = new Blob([bytes], {
            type: "application/json;charset=utf-8"
        });

        var file = new File([blob1], "CallBackScript_" + orgDetails.name);

        var data = new FormData();
        data.append('file', file);
        data.append('scriptName', "CallBackScript_" + orgDetails.name);
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'https://apps.' + localStorage.getItem(this.integrationType + ":environment") + '/uploads/v2/scripter',
                type: 'POST',
                headers: {
                    Authorization: 'bearer ' + localStorage.getItem(this.integrationType + ':accessToken')
                },
                data: data,
                processData: false,
                contentType: false,
                success: resolve,
                error: function (xhr, status, error) {
                    console.log('add Script Instance, xhr = ' + JSON.stringify(xhr, null, 3));
                    console.log('add Script Instance, status = ' + JSON.stringify(status, null, 3));
                    console.log('add Script Instance, error = ' + JSON.stringify(error, null, 3));

                    reject("Error in adding Script Instance.");
                }
            })
        });
    }

    //// =======================================================
    ////      INTERACTION WIDGET
    //// =======================================================
    addIntegration() {
        this.logInfo('Creating Interaction Widget ' + this.appName);
        let opts = {
            body: {} // integration config current
        };

        opts.body = {
            "name": this.appName,
            "integrationType": {
                "id": "embedded-client-app-interaction-widget",
                "name": this.appName
            }
        };

        return this.integrationsApi.postIntegrations(opts);
    }

    updateIntegration(integrationData) {
        this.logInfo('Created Interaction Widget ' + this.appName);

        return Promise.all([integrationData.id, this.integrationsApi.getIntegrationConfigCurrent(integrationData.id)])
            .then(values => {
                console.log('App instance current config: ' + JSON.stringify(values[1], null, 3));
                this.logInfo('Updating Interaction Widget ' + this.appName);

                let integrationsOpts = {
                    body: values[1]  // integration config current
                };

                integrationsOpts.body.name = this.appName;
                integrationsOpts.body.properties.url = this.appUrl + "CallBack.html?conversationId={{pcConversationId}}&environment=" + localStorage.getItem(this.integrationType + ":environment");
                integrationsOpts.body.properties.communicationTypeFilter = "callback";

                console.log('Integration instance new config: ' + JSON.stringify(integrationsOpts, null, 3));

                return this.integrationsApi.putIntegrationConfigCurrent(values[0], integrationsOpts);
            });
    }

    //// =======================================================
    ////      DISPLAY/UTILITY FUNCTIONS
    //// =======================================================

    /**
     * Renders the proper text language into the web pages
     * @param {Object} text  Contains the keys and values from the language file
     */
    displayPageText(text) {
        $(document).ready(() => {
            for (let key in text) {
                if (!text.hasOwnProperty(key)) continue;
                $("." + key).text(text[key]);
            }
        });
    }

    /**
     * Shows an overlay with the specified data string
     * @param {string} data 
     */
    logInfo(data) {
        if (!data || (typeof (data) !== 'string')) data = "";

        $.LoadingOverlay("text", data);
    }

    //// =======================================================
    ////      ENTRY POINT
    //// =======================================================
    start() {
        return new Promise((resolve, reject) => {
            this._setupClientApp()
                .then(() => this._pureCloudAuthenticate())
                .then((data) => {
                    // console.log('start, data = ', JSON.stringify(data, null, 3));
                    if (data && data.accessToken) {
                        localStorage.setItem(this.integrationType + ':accessToken', data.accessToken);
                    }
                    console.log('Setup success');
                    return resolve();
                })
                .catch((err) => {
                    console.log(err);
                    reject(err)
                });
        });
    }

    /**
     * First thing that needs to be called to setup up the PureCloud Client App
     */
    _setupClientApp() {
        // Snippet from URLInterpolation example: 
        // https://github.com/MyPureCloud/client-app-sdk
        const queryString = window.location.search.substring(1);
        const pairs = queryString.split('&');

        let pcEnv = null;
        let langTag = null;

        for (let i = 0; i < pairs.length; i++) {
            const currParam = pairs[i].split('=');

            if (currParam[0] === 'langTag') {
                langTag = currParam[1];
            } else if (currParam[0] === 'environment') {
                pcEnv = currParam[1];
            }
        }

        // Stores the query parameters into localstorage
        // If query parameters are not provided, try to get values from localstorage
        // Default values if it does not exist.
        if (pcEnv) {
            localStorage.setItem(this.integrationType + ":environment", pcEnv);
        } else if (localStorage.getItem(this.integrationType + ":environment")) {
            pcEnv = localStorage.getItem(this.integrationType + ":environment");
        } else {
            // Use default PureCloud region
            pcEnv = appConfig.defaultPcEnv;
            localStorage.setItem(this.integrationType + ":environment", pcEnv);
        }

        this.pcEnvironment = pcEnv;
        console.log("Environment:" + this.pcEnvironment);

        if (langTag) {
            localStorage.setItem(this.integrationType + ":langTag", langTag);
        } else if (localStorage.getItem(this.integrationType + ":langTag")) {
            langTag = localStorage.getItem(this.integrationType + ":langTag");
        } else {
            // Use default Language
            langTag = appConfig.defaultLangTag;
        }
        this.language = langTag;

        console.log("Language:" + this.language);

        // Get the language context file and assign it to the app
        // For this example, the text is translated on-the-fly.
        return new Promise((resolve, reject) => {
            const fileUri = '../languages/' + this.language + '.json';
            $.getJSON(fileUri)
                .done(data => {
                    //this.displayPageText(data);
                    resolve();
                })
                .fail(xhr => {
                    reject(new Error(`Language file not found - "${this.language}.json"`));
                });
        });
    }

    /**
     * Authenticate to PureCloud (Implicit Grant)
     * @return {Promise}
     */
    _pureCloudAuthenticate() {
        console.log('_pureCloudAuthenticate clientId =', appConfig.clientId);
        console.log('_pureCloudAuthenticate redirectUri =', this.redirectUri);
        console.log('_pureCloudAuthenticate pcEnvironment =', this.pcEnvironment);

        this.purecloudClient.setEnvironment(this.pcEnvironment);
        return this.purecloudClient.loginImplicitGrant(
            appConfig.clientId,
            this.redirectUri,
            { state: ('pcEnvironment=' + this.pcEnvironment) });
    }
}


export default WizardApp;